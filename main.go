package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
)

const (
	UserName  = "realssd"
	FuncLabel = "testgo"
)

func main() {
	relativePath := fmt.Sprintf("/call/%s/%s", UserName, FuncLabel)
	r := gin.Default()
	g := r.Group(relativePath)
	g.GET("/ping", func(context *gin.Context) {
		context.String(200, "PONG!")
		return
	})
	err := r.Run(":8080")
	if err != nil {
		panic(err)
	}
}
